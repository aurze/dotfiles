#!/bin/bash

current_song=`playerctl metadata title | tr '"' ' ' | sed 's/&/&amp;/g'`
tooltip=`playerctl metadata --format "{{ playerName }} : {{ artist }} - {{ title }}" | tr '"' ' ' | sed 's/&/&amp;/g'`

echo '{"text": "'$current_song'", "alt": "'$(playerctl status)'", "tooltip": "'$tooltip'", "class": "'$(playerctl status)'" }'
