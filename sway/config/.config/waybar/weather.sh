#!/bin/sh

result=`curl --silent 'https://wttr.in/Chambly?format=1'`

temp=`echo $result | grep -o \[+-\]\.\*`
icon=":("

case $result in
  *"☀️"*)
    icon="sun"
    ;;
  *"⛅️"*)
    icon="cloud+sun"
    ;;
esac

echo '{ "alt": "'$icon'",  "text": "'$temp'" }'
# curl 'https://wttr.in/Chambly?format=1' | sed s/☀/'{ \"alt\": \"\", \"text\": \"'/ | sed s/'\(.*\)'/'\1\" }'/
