#! /bin/bash

install_package() {
	pikaur -Suy --needed $@
}

install_packages() {
	packages=$(./$1/requirement.sh)

	if [ -f ./$1/preinstall.sh ]; then
		./$1/preinstall.sh
	fi
	
	install_package $packages

	xstow -t ${HOME} -d $1 config

	if [ -f ./$1/install.sh ]; then
		./$1/install.sh
	fi
}

install_packages $1

