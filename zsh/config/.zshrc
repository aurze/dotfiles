source ~/.bashrc

# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:/usr/local/bin:$PATH
export PATH="$(ruby -e 'print Gem.user_dir')/bin:$PATH"
export POWERLINE_CONFIG_COMMAND=/usr/bin/powerline-config

alias diff='diff --color=auto'
alias grep='grep --color=auto'
alias vimdiff='nvim -d'

unalias duf # it conflict with duf


 man () {
LESS_TERMCAP_mb=$'\e'"[1;31m" \
LESS_TERMCAP_md=$'\e'"[1;31m" \
LESS_TERMCAP_me=$'\e'"[0m" \
LESS_TERMCAP_se=$'\e'"[0m" \
LESS_TERMCAP_so=$'\e'"[1;44;33m" \
LESS_TERMCAP_ue=$'\e'"[0m" \
LESS_TERMCAP_us=$'\e'"[1;32m" \
command man "$@"
}

bindkey -v
function zle-line-init zle-keymap-select {
    VIM_PROMPT="%{$fg_bold[yellow]%} [% NORMAL]%  %{$reset_color%}"
    RPS1="${${KEYMAP/vicmd/$VIM_PROMPT}/(main|viins)/}$(git_custom_status) $EPS1"
    zle reset-prompt
}

#zle -N zle-line-init
#zle -N zle-keymap-select
export KEYTIMEOUT=1

setopt NO_GLOBAL_RCS

go () {
  mkdir "$1"
  cd "$1"
}

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.config/oh-my-zsh"

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="amuse"

plugins=(aliases command-not-found common-aliases docker docker-compose git gitignore tmux)

source $ZSH/oh-my-zsh.sh

eval "$(starship init zsh)"

eval $(thefuck --alias)
