#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#if command -v tmux &> /dev/null && [ -n "$PS1" ] && [[ ! "$TERM" =~ screen ]] && [[ ! "$TERM" =~ tmux ]] && [ -z "$TMUX" ]; then
#  exec tmux
#fi

alias ls='ls_extended'
alias cp="rsync -ah --inplace --no-whole-file --info=progress2"
# PS1='[\u@\h \W]\$ '

# pywal color scheme
#(cat ~/.cache/wal/sequences &)

export VISUAL="nvim"
export EDITOR="$VISUAL"

export TERM=screen-256color

export PATH=$PATH":$HOME/bin:$HOME/.local/bin"

export QT_QPA_PLATFORMTHEME=qt5ct
export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_HOME=$HOME/.local/share

export XDG_CONFIG_DIRS=/etc/xdg

export CC=clang
export CXX=clang++
export LDFLAGS=-fuse-ld=lld

alias cat=bat
alias swap-refresh="sudo swapoff -a && sudo swapon -a"
