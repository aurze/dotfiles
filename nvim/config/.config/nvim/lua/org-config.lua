require('orgmode').setup({
  org_agenda_file = {'~/my-orgs/**/*'},
  org_default_notes_file = '~/my-orgs/refile.org',
})
